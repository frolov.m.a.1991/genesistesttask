module "create-vm" {
  source  = "./vm"
  name    = "web"
  vm_type = "t2.small"
  ami     = "ami-0245697ee3e07e755"
  vpc_id  = "vpc-53d96239"
}