output "vm-id" {
  value = aws_instance.virtual_machine.id
}

output "ec2instance" {
  value = aws_instance.virtual_machine.public_ip
}