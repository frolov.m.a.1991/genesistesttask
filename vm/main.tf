resource "aws_security_group" "genesis_security_group" {
  name = "Genesis-Sec-Group"
  description = "Genesis-Sec-Group"
  vpc_id = var.vpc_id

  // To Allow SSH Transport
  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  // To Allow Port 80 Transport
  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  // To Allow Port 443 Transport
  ingress {
    from_port = 443
    protocol = "tcp"
    to_port = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_instance" "virtual_machine" {
  instance_type = var.vm_type
  ami = var.ami
  associate_public_ip_address = true
  key_name = "GenesisTestTask"

  vpc_security_group_ids = [
      aws_security_group.genesis_security_group.id
  ]
  
  tags = {
      "Name" = var.name
      "Environment" = "DEV"
      "OS" = "UBUNTU"
      "Managed" = "IAC"
  }

  depends_on = [ aws_security_group.genesis_security_group ]
}