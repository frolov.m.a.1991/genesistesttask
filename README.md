# GenesisTestTask



## The list of goals

### Part one (Terraform)
1. Using terraform create AWS EC2 instance(t2.small, debian10) in own account. P.S. security, IAM for your discretion.

### Part two (Ansible)
1. Using ansible on the previously created instance install the necessary packages for work.
2. Using ansible install and configure: Nginx, MySQL, PHP7.4
3. Configure TLS connection with Nginx in any way.
4. In result of TestTask need to get phpinfo()-page on a previously configured domain name.

### Part three
1. Upload the code written for the TestTask to the personal open GitLab repository.
2. Provide a link to:
- GitLab with the source code of the TestTask
- Domain for checking the results of the test task
3. Install http-check domain in any convenient way and attach a screen of successful domain availability check.

## My adventure

### Part one
I try discover as many as I can possibilities of terraform. I use terraform module for creating vm instance and security group with ingress and egress rules + dependency lifecycle + variables, write terraform state in S3 bucket and also add nice output =)

### Part two
In this part I spent almost all time =)
I use templates on jinja2 for info.php page and for nginx configure, file variables(vars/default.yml) for configure mysql and nginx, and the main part described in playbook(installation and configure all packages + firewall rules), using key pairs for connection to the instance. Also I create a free domain and set up tls with letsencrypt. And of course a lot of falls in installation packages =)

### Part three
Not clear for now