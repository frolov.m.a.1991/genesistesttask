terraform {
  backend "s3" {
    region  = "eu-central-1"
    bucket  = "genesis-terraform-maksym-frolov-tf"
    key     = "terraform_iam_user.tfstate"
    profile = "terraform_iam_user"
  }
}